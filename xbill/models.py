from django.db import models


class ScoreQuerySet(models.QuerySet):
    def high_scores(self):
        return self.order_by("-score")[:10]

    def lowest_score(self):
        return self.order_by("score").values_list("score", flat=True)[0]


class Score(models.Model):

    name = models.CharField(max_length=20)
    level = models.PositiveSmallIntegerField()
    score = models.PositiveIntegerField()

    objects = ScoreQuerySet.as_manager()

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save()
