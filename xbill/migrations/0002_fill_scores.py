# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def fill_scores(apps, schema_editor):
    Score = apps.get_model("xbill", "Score")
    db_alias = schema_editor.connection.alias

    Score.objects.using(db_alias).bulk_create(
        [Score(name="Anonymous", level=0, score=0)] * 10
    )


def reset_scores(apps, schema_editor):
    Score = apps.get_model("xbill", "Score")
    db_alias = schema_editor.connection.alias

    Score.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [("xbill", "0001_initial")]

    operations = [migrations.RunPython(fill_scores, reset_scores)]
