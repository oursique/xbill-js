from django import forms

from . import models


class ScoreForm(forms.ModelForm):
    class Meta:
        model = models.Score
        fields = ("name", "level", "score")
