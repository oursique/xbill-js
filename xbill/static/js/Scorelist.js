;(function (exports) {
  var NAMELEN = 20;
  var SCORES = 10;

  var scores = new Array(SCORES);

  var Scorelist = {};
  exports.Scorelist = Scorelist;

  Scorelist.read = function () {
    // Empty scores while we get the read list from the server
    for (var i = 0; i < SCORES; i++) {
      scores[i] = {};
      scores[i].name = "Anonymous";
      scores[i].level = 0;
      scores[i].score = 0;
    }

    // Get the authoritative list from the server
    $.getJSON(SCOREFILE_URL, {}, function (scorefile) {
      scores = scorefile;
      Scorelist.update();
    });
  };

  /*  Add new high score to list   */
  Scorelist.recalc = function (str, level, score) {
    if (scores[SCORES - 1].score >= score) {
      return;
    }
    for (var i = SCORES - 1; i > 0; i--) {
      if (scores[i - 1].score < score) {
        scores[i].name = scores[i - 1].name;
        scores[i].level = scores[i - 1].level;
        scores[i].score = scores[i - 1].score;
      }
      else {
        break;
      }
    }

    if (str) {
      str = str.trim().replace("\n", "").substr(0, NAMELEN);
    }

    scores[i].name = str ? str : "Anonymous";
    scores[i].level = level;
    scores[i].score = score;

    // Send to the server
    $.post(SCOREFILE_URL, {name: str, level: level, score: score});
  };

  Scorelist.update = function () {
    var table = document.createElement('table');

    var thead = document.createElement('thead');
    table.appendChild(thead);

    var row = document.createElement('tr');
    thead.appendChild(row);

    var nameCell = document.createElement('th');
    nameCell.textContent = "Name";
    thead.appendChild(nameCell);

    var levelCell = document.createElement('th');
    levelCell.textContent = "Level";
    thead.appendChild(levelCell);

    var scoreCell = document.createElement('th');
    scoreCell.textContent = "Score";
    thead.appendChild(scoreCell);

    var tbody = document.createElement('tbody');
    table.appendChild(tbody);

    for (var i = 0; i < SCORES; i++) {
      row = document.createElement('tr');
      tbody.appendChild(row);

      nameCell = document.createElement('td');
      nameCell.textContent = scores[i].name;
      row.appendChild(nameCell);

      levelCell = document.createElement('td');
      levelCell.textContent = scores[i].level;
      row.appendChild(levelCell);

      scoreCell = document.createElement('td');
      scoreCell.textContent = scores[i].score;
      row.appendChild(scoreCell);
    }

    // Replace the table
    var modal = document.getElementById('highScoresModal');
    var old_table = modal.getElementsByTagName('table')[0];
    var parent = old_table.parentNode;
    // Save css classes to reapply them
    var css_classes = old_table.getAttribute('class');
    parent.removeChild(old_table);
    table.setAttribute('class', css_classes);
    parent.appendChild(table);
  };

  Scorelist.ishighscore = function (val) {
    return (val > scores[SCORES - 1].score);
  };
})(this);
