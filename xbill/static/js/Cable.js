;(function (exports) {

  var Cable = function () {
    /* computers connected */
    this.c1 = 0;
    this.c2 = 0;
    /* endpoints of line representing cable */
    this.x1 = 0;
    this.y1 = 0;
    this.x2 = 0;
    this.y2 = 0;
    /* current location of spark */
    this.x = 0;
    this.y = 0;
    /* needed for line drawing */
    this.fx = 0;
    this.fy = 0;
    /* is spark moving and from which end */
    this.active = false;
    this.index = 0;
  };
  exports.Cable = Cable;

  Cable.setup = function () {
    var cable = new Cable();

    cable.c1 = RAND(0, Network.num_computers() - 1);
    do {
      cable.c2 = RAND(0, Network.num_computers() - 1);
    } while (cable.c2 == cable.c1);
    cable.active = false;
    cable.index = 0;

    var comp1 = Network.get_computer(cable.c1);
    var comp2 = Network.get_computer(cable.c2);
    var cwidth = Computer.width();
    var cheight = Computer.height();
    cable.x1 = Math.floor(comp1.x + cwidth / 3);
    cable.x2 = Math.floor(comp2.x + cwidth / 3);
    cable.y1 = Math.floor(comp1.y + cheight / 2);
    cable.y2 = Math.floor(comp2.y + cheight / 2);

    return cable;
  };

  Cable.prototype.reverse = function () {
    var t;

    t = this.c1;
    this.c1 = this.c2;
    this.c2 = t;

    t = this.x1;
    this.x1 = this.x2;
    this.x2 = t;

    t = this.y1;
    this.y1 = this.y2;
    this.y2 = t;
  };

  Cable.prototype.draw = function () {
    UI.draw_line(this.x1, this.y1, this.x2, this.y2);
    if (this.active) {
      var rx = Math.floor(this.x - Spark.width() / 2);
      var ry = Math.floor(this.y - Spark.height() / 2);
      Spark.draw(rx, ry, this.index);
    }
  };

  Cable.prototype.update = function () {
    var comp1 = Network.get_computer(this.c1);
    var comp2 = Network.get_computer(this.c2);

    if (this.active) {
      if ((comp1.os == OS_WINGDOWS) == (comp2.os == OS_WINGDOWS)) {
        this.active = false;
      }
      else if (comp1.os == OS_WINGDOWS || comp2.os == OS_WINGDOWS) {
        if (comp2.os == OS_WINGDOWS) {
          this.reverse();
        }

        var xdist = this.x2 - this.x;
        var ydist = this.y2 - this.y;

        var sx = xdist >= 0 ? 1.0 : -1.0;
        var sy = ydist >= 0 ? 1.0 : -1.0;
        xdist = abs(xdist);
        ydist = abs(ydist);
        if (xdist == 0 && ydist == 0) {
          if (!comp2.busy) {
            var counter;
            if (comp2.os == OS_OFF) {
              counter = NETWORK_COUNTER_OFF;
            }
            else {
              counter = NETWORK_COUNTER_BASE;
            }
            Network.inc_counter(counter, -1);
            Network.inc_counter(NETWORK_COUNTER_WIN, 1);
            comp2.os = OS_WINGDOWS;
            Sound.play(SOUND_WINGDOWS_INSTALL);
          }
          this.active = false;
        }
        else if (Math.max(xdist, ydist) < SPARK_SPEED) {
          this.x = this.x2;
          this.y = this.y2;
        }
        else {
          this.fx += (xdist * SPARK_SPEED * sx) / (xdist + ydist);
          this.fy += (ydist * SPARK_SPEED * sy) / (xdist + ydist);
          this.x = Math.floor(this.fx);
          this.y = Math.floor(this.fy);
        }
        this.index = 1 - this.index;
      }
    }
    else {
      if ((comp1.os == OS_WINGDOWS) == (comp2.os == OS_WINGDOWS)) {
      }
      else if (comp1.os == OS_WINGDOWS || comp2.os == OS_WINGDOWS) {
        this.active = true;
        Sound.play(SOUND_SPARK_FIRE);
        if (comp2.os == OS_WINGDOWS) {
          this.reverse();
        }
        this.x = this.x1;
        this.fx = this.x1;
        this.y = this.y1;
        this.fy = this.y1;
      }
    }
  };

  Cable.prototype.onspark = function (locx, locy) {
    if (!this.active) {
      return 0;
    }
    return (abs(locx - this.x) < Spark.width() && abs(locy - this.y) < Spark.height());
  };

  Cable.prototype.reset = function () {
    this.active = false;
  };
})(this);
