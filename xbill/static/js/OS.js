var OS_WINGDOWS = 0;  // OS 0 is wingdows
var OS_OFF = -1;  // OS -1 means the computer is off
var OS_PC = 6;  // OS >= PC means the OS is a PC OS

(function (exports) {
  var MIN_PC = 6;  // OS >= MIN_PC means the OS is a PC OS

  var osname = ["wingdows", "apple", "next", "sgi", "sun", "palm", "os2", "bsd", "linux", "redhat", "hurd"];
  var NUM_OS = osname.length;

  var os = new Array(NUM_OS);  // array of OS pictures
  var cursor = new Array(NUM_OS);  // array of OS cursors (drag/drop)

  var OS = {};
  exports.OS = OS;

  OS.load_pix = function () {
    for (var i = 0; i < NUM_OS; i++) {
      os[i] = UI.load_picture(osname[i]);
      if (i != 0) {
        cursor[i] = UI.load_picture(osname[i]);
      }
    }
  };

  OS.draw = function (index, x, y) {
    UI.draw(os[index], x, y);
  };

  OS.width = function () {
    return UI.picture_width(os[0]);
  };

  OS.height = function () {
    return UI.picture_height(os[0]);
  };

  OS.set_cursor = function (index) {
    UI.set_cursor(cursor[index]);
  };

  OS.randpc = function () {
    return (RAND(MIN_PC, NUM_OS - 1));
  };

  OS.ispc = function (index) {
    return (index >= MIN_PC);
  }
})(this);
