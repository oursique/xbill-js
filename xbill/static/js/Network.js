/* Counters */
var NETWORK_COUNTER_OFF = 0;
var NETWORK_COUNTER_BASE = 1;
var NETWORK_COUNTER_WIN = 2;
var NETWORK_COUNTER_MAX = 2;

(function (exports) {
  var STD_MAX_COMPUTERS = 20;

  var computers = [];
  var ncomputers = 0;
  var cables = [];
  var ncables = 0;
  var counters = new Array(NETWORK_COUNTER_MAX + 1);  // number in each state

  function on(level) {
    var normal = Math.min(8 + level, STD_MAX_COMPUTERS);
    return Math.floor(normal * Game.scale(2));
  }

  var Network = {};
  exports.Network = Network;

  /* sets up network for each level */
  Network.setup = function () {
    var i;
    var computer;

    ncomputers = on(Game.level());
    computers = [];
    for (i = 0; i < ncomputers; i++) {
      try {
        computers[i] = Computer.setup(i);
      }
      catch (err) {
        log("error in creating computer:" + err);
        ncomputers = i - 1;
        break;
      }
    }
    counters[NETWORK_COUNTER_OFF] = 0;
    counters[NETWORK_COUNTER_BASE] = ncomputers;
    counters[NETWORK_COUNTER_WIN] = 0;
    ncables = Math.min(Game.level(), Math.floor(ncomputers / 2));
    cables = [];
    for (i = 0; i < ncables; i++) {
      cables[i] = Cable.setup();
    }
  };

  /* redraws the computers at their location with the proper image */
  Network.draw = function () {
    var i;
    for (i = 0; i < ncables; i++) {
      cables[i].draw();
    }
    for (i = 0; i < ncomputers; i++) {
      computers[i].draw();
    }
  };

  Network.update = function () {
    for (var i = 0; i < ncables; i++) {
      cables[i].update();
    }
  };

  Network.toasters = function () {
    for (var i = 0; i < ncomputers; i++) {
      computers[i].type = COMPUTER_TOASTER;
      computers[i].os = OS_OFF;
    }
    ncables = 0;
  };

  Network.get_computer = function (index) {
    return computers[index];
  };

  Network.num_computers = function () {
    return ncomputers;
  };

  Network.get_cable = function (index) {
    return cables[index];
  };

  Network.num_cables = function () {
    return ncables;
  };

  Network.clear_stray = function (bill) {
    for (var i = 0; i < ncomputers; i++) {
      if (computers[i].stray == bill) {
        computers[i].stray = null;
      }
    }
  };

  Network.inc_counter = function (counter, val) {
    counters[counter] += val;
  };

  Network.get_counter = function (counter) {
    return counters[counter];
  }

})(this);
