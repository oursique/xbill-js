var DIALOG_NEWGAME = 0;
var DIALOG_PAUSEGAME = 1;
var DIALOG_WARPLEVEL = 2;
var DIALOG_HIGHSCORE = 3;
var DIALOG_QUITGAME = 4;
var DIALOG_STORY = 5;
var DIALOG_RULES = 6;
var DIALOG_ABOUT = 7;
var DIALOG_SCORE = 8;
var DIALOG_ENDGAME = 9;
var DIALOG_ENTERNAME = 10;
var DIALOG_MAX = 10;

(function (exports) {
  var playing = false;

  /* These are for the UI implementation */
  var BACKGROUND_COLOR = makecol(255, 255, 255);
  var LINE_COLOR = makecol(0, 0, 0);
  var FONT_SIZE = 12;
  var FONT_COLOR = makecol(0, 0, 0);
  var current_cursor;
  var timer_active = false;

  /*
   * Timer control routines
   */

  function timer_tick() {
    Game.update();
  }

  var UI = {};
  exports.UI = UI;

  UI.restart_timer = function () {
    install_int(timer_tick, 200);
    timer_active = true;
  };

  UI.kill_timer = function () {
    remove_all_ints();
    timer_active = false;
  };

  // TODO call if game looses focus
  UI.pause_game = function () {
    if (timer_active) {
      playing = true;
    }
    this.kill_timer();
  };

  // TODO call if game gains focus
  UI.resume_game = function () {
    if (playing && !timer_active) {
      this.restart_timer();
    }
    playing = false;
  };

  /*
   * Window routines
   */

  UI.initialize = function () {
    if (DEBUG) {
      enable_debug('debug');
      ALLEGRO_CONSOLE = true;  // Log to browser console
    }
  };

  UI.make_main_window = function (size) {
    allegro_init_all('game_canvas', size, size);
    // Except the keyboard (we don't use it and it prevents browser shortcuts)
    remove_keyboard();
  };

  UI.graphics_init = function () {
  };

  UI.popup_dialog = function (dialog) {
    var options = {backdrop: false, keyboard: false};

    switch (dialog) {
      case DIALOG_ENDGAME:
        $('#endGameModal').modal(options);
        break;
      case DIALOG_ENTERNAME:
        $('#enterNameModal').modal(options);
        break;
      case DIALOG_HIGHSCORE:
        $('#highScoresModal').modal(options);
        break;
      case DIALOG_SCORE:
        $('#scoreModal').modal(options);
        break;
    }
  };

  /*
   * Graphics routines
   */

  UI.set_cursor = function (cursor) {
    current_cursor = cursor;
    cursor ? hide_mouse() : show_mouse();
  };

  UI.clear = function () {
    clear_to_color(canvas, BACKGROUND_COLOR);
  };

  UI.refresh = function () {
    /*
     * Was called for double buffering
     * Keep it to draw the cursor on the top of the canvas
     */
    if (current_cursor) {
      draw_sprite(canvas, current_cursor, mouse_x - Math.floor(current_cursor.w / 2), mouse_y - Math.floor(current_cursor.h / 2));
    }
  };

  UI.draw = function (pict, x, y) {
    draw_sprite(canvas, pict, x, y);
  };

  UI.draw_line = function (x1, y1, x2, y2) {
    line(canvas, x1, y1, x2, y2, LINE_COLOR, 2);
  };

  UI.draw_str = function (str, x, y) {
    textout(canvas, font, str, x, y, FONT_SIZE, FONT_COLOR);
  };

  /*
   * Other routines
   */

  UI.set_pausebutton = function (enabled) {
    $('.inGameButtons').attr('disabled', !enabled);
  };

  UI.load_picture_indexed = function (name, index) {
    if (index > 99) {
      _error("image index too large");
    }
    var newname = name + "_" + index;
    return this.load_picture(newname);
  };

  UI.load_picture = function (name) {
    return load_bitmap(STATIC_URL + "pixmaps/" + name + ".png");
  };

  UI.picture_width = function (pict) {
    return pict.w;
  };

  UI.picture_height = function (pict) {
    return pict.h;
  };

  UI.load_cursor = function (name) {
    return load_bitmap(STATIC_URL + "bitmaps/" + name + ".png");
  };

  UI.intersect = function (x1, y1, w1, h1, x2, y2, w2, h2) {
    return (abs(x2 - x1 + (w2 - w1) / 2) < (w1 + w2) / 2) && (abs(y2 - y1 + (h2 - h1) / 2) < (h1 + h2) / 2);
  };

  UI.update_dialog = function (index, str) {
    switch (index) {
      case DIALOG_SCORE:
        $('#scoreModal').find('p').text(str);
        break;
    }
  };
})(this);

$(document).ready(function () {

  /*
   * Set the behaviour of modal dialogs
   */
  var modals = $('.modal');

  // Pause the game as soon as a modal is triggered
  modals.on('show.bs.modal', function () {
    UI.pause_game();
  });

  // Focus the (only) input field if any
  modals.on('shown.bs.modal', function () {
    $(this).find('input').focus();
  });

  // Resume the game after the modal has disappeared
  modals.on('hidden.bs.modal', function () {
    UI.resume_game();
  });

  /*
   * Moved from the game loop when the game ends because JS is asynchronous
   */

  $('#newGameModal').find('.btn-primary').click(function () {
    Game.cb_after_newgame();
  });

  $('#quitGameModal').find('.btn-primary').click(function () {
    Game.cb_after_quitgame();
  });

  $('#warpLevelModal').find('.btn-primary').click(function () {
    var modal = $(this).parents('.modal');
    var level = modal.find('input').val();
    Game.cb_after_warplevel(level);
  });

  $('#scoreModal').find('.btn-primary').click(function () {
    Game.cb_after_score();
  });

  $('#endGameModal').find('.btn-primary').click(function () {
    Game.cb_after_end();
  });

  var enterNameModal = $('#enterNameModal');

  // When validated
  enterNameModal.find('.btn-primary').click(function () {
    var modal = $(this).parents('.modal');
    var name = modal.find('input').val();
    Game.cb_after_entername(name);
  });

  // When cancelled
  enterNameModal.find('.btn-default').click(function () {
    Game.cb_after_entername("")
  });

  $('#highScoresModal').find('.btn-primary').click(function () {
    Game.cb_after_highscores();
  });

  /*
   * Sound
   */

  $('#muteButton').click(function () {
    if (get_volume() > 0) {
      Sound.mute();
      this.textContent = "Unmute";
    }
    else {
      Sound.unmute();
      this.textContent = "Mute";
    }
  });

  /*
   * Score sending
   */

  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  var csrftoken = getCookie('csrftoken');

  $.ajaxSetup({
    beforeSend: function (xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });
});
