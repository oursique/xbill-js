;(function (exports) {
  var picture;
  var cursor;
  var grabbed;

  var Bucket = {};
  exports.Bucket = Bucket;

  Bucket.load_pix = function () {
    picture = UI.load_picture("bucket");
    cursor = UI.load_picture("bucket");
  };

  Bucket.clicked = function (x, y) {
    return (x > 0 && x < UI.picture_width(picture) && y > 0 && y < UI.picture_height(picture));
  };

  Bucket.draw = function () {
    if (!grabbed) {
      UI.draw(picture, 0, 0);
    }
  };

  Bucket.grab = function (x, y) {
    UI.set_cursor(cursor);
    grabbed = true;
    Sound.play(SOUND_BUCKET_GRAB);
  };

  Bucket.release = function (x, y) {
    for (var i = 0; i < Network.num_cables(); i++) {
      var cable = Network.get_cable(i);
      if (cable.onspark(x, y)) {
        cable.reset();
        Sound.play(SOUND_SPARK_PUTOUT);
      }
    }
    grabbed = false;
  };
})(this);
