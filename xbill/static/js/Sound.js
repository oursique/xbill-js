var SOUND_BILL_ENTER = 0;
var SOUND_BILL_KILL = 1;
var SOUND_BUCKET_GRAB = 2;
var SOUND_SPARK_FIRE = 3;
var SOUND_SPARK_PUTOUT = 4;
var SOUND_OS_REMOVE = 5;
var SOUND_OS_INSTALL = 6;
var SOUND_WINGDOWS_INSTALL = 7;

(function (exports) {
  var sounds = [];
  var names = [
    "bill_enter", "bill_kill", "bucket_grab", "spark_fire", "spark_putout", "os_remove", "os_install",
    "wingdows_install"
  ];

  var Sound = {};
  exports.Sound = Sound;

  Sound.load = function () {
    for (var i = 0; i < names.length; i++) {
      sounds[i] = load_sample(STATIC_URL + "sounds/" + names[i] + ".mp3");

    }
  };

  Sound.mute = function () {
    set_volume(0);
  };

  Sound.unmute = function () {
    set_volume(1.0);
  };

  Sound.play = function (index) {
    play_sample(sounds[index]);
  };

})(this);
