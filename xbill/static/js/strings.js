/*
 * Dialog boxes
 */

var newgame_dialog_str = "New Game?";
var pause_dialog_str = "Game paused.  Press Continue to continue.";
var warp_dialog_str = "Warp to level?";
var quit_dialog_str = "Quit Game?";

var endgame_dialog_str =
  "Module xBill has caused a segmentation fault\n\
  at memory address 097E:F1A0.  Core dumped.\n\
  \n\
  We apologize for the inconvenience.";

var entername_dialog_str = "You earned a high score.\nEnter your name:";


/*
 * Menus
 */
var newgame_menu_str = "New Game";
var pause_menu_str = "Pause Game";
var warp_menu_str = "Warp to level...";
var highscore_menu_str = "View High Scores";
var quit_menu_str = "Quit Game";

var score_menu_str = "Score";
var endgame_menu_str = "End Game";
var entername_menu_str = "Enter Name";
