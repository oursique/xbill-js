/* Game states */
var STATE_PLAYING = 1;
var STATE_BETWEEN = 2;
var STATE_END = 3;
var STATE_WAITING = 4;

(function (exports) {
  var SCREENSIZE = 400;

  /* Score related constants */
  var SCORE_BILLPOINTS = 5;

  var state = STATE_WAITING;
  var efficiency = 0;
  var score = 0, level = 0, iteration = 0;
  var logo;
  var defaultcursor, downcursor;
  var grabbed;
  var screensize = SCREENSIZE;

  function setup_level(newlevel) {
    level = newlevel;
    Horde.setup();
    grabbed = null;
    UI.set_cursor(defaultcursor);
    Network.setup();
    iteration = 0;
    efficiency = 0;
  }

  var Game = {};
  exports.Game = Game;

  Game.start = function (newlevel) {
    state = STATE_PLAYING;
    score = 0;
    UI.restart_timer();
    UI.set_pausebutton(true);
    setup_level(newlevel);
  };

  Game.quit = function () {
    state = STATE_END;  // Instead of exiting the process, we consider an abandon
  };

  function update_info() {
    var on_screen = Horde.get_counter(HORDE_COUNTER_ON);
    var off_screen = Horde.get_counter(HORDE_COUNTER_OFF);
    var base = Network.get_counter(NETWORK_COUNTER_BASE);
    var off = Network.get_counter(NETWORK_COUNTER_OFF);
    var win = Network.get_counter(NETWORK_COUNTER_WIN);
    var units = Network.num_computers();
    var str = "Bill: {0}/{1}  System: {2}/{3}/{4}  Level: {5}  Score: {6}".format(
      on_screen, off_screen, base, off, win, level, score
    );
    UI.draw_str(str, 5, screensize - 5);
    efficiency += Math.floor((100 * base - 10 * win) / units);
  }

  Game.warp_to_level = function (lev) {
    if (state == STATE_PLAYING) {
      if (lev <= level) {
        return;
      }
      setup_level(lev);
    }
    else {
      if (lev <= 0) {
        return;
      }
      this.start(lev);
    }
  };

  Game.add_high_score = function (str) {
    Scorelist.recalc(str, level, score);
  };

  Game.button_press = function (x, y) {
    if (state != STATE_PLAYING) {
      return;
    }
    UI.set_cursor(downcursor);

    if (Bucket.clicked(x, y)) {
      Bucket.grab(x, y);
      return;
    }

    grabbed = Horde.clicked_stray(x, y);
    if (grabbed != null) {
      OS.set_cursor(grabbed.cargo);
      return;
    }

    var counter = Horde.process_click(x, y);
    score += (counter * counter * SCORE_BILLPOINTS);
  };

  Game.button_release = function (x, y) {
    UI.set_cursor(defaultcursor);

    if (state != STATE_PLAYING) {
      return;
    }

    if (grabbed == null) {
      Bucket.release(x, y);
      return;
    }

    for (var i = 0; i < Network.num_computers(); i++) {
      var computer = Network.get_computer(i);

      if (computer.on(x, y) && computer.compatible(grabbed.cargo) && (computer.os == OS_WINGDOWS || computer.os == OS_OFF)) {
        var counter;

        Network.inc_counter(NETWORK_COUNTER_BASE, 1);
        if (computer.os == OS_WINGDOWS) {
          counter = NETWORK_COUNTER_WIN;
        }
        else {
          counter = NETWORK_COUNTER_OFF;
        }
        Network.inc_counter(counter, -1);
        computer.os = grabbed.cargo;
        Horde.remove_bill(grabbed);
        grabbed = null;
        return;
      }
    }
    Horde.add_bill(grabbed);
    grabbed = null;
  };

  Game.draw_logo = function () {
    UI.clear();
    UI.draw(logo,
      Math.floor(screensize - UI.picture_width(logo)) / 2,
      Math.floor(screensize - UI.picture_height(logo)) / 2);
  };

  Game.update = function () {
    switch (state) {
      case STATE_PLAYING:
        UI.clear();
        Bucket.draw();
        Network.update();
        Network.draw();
        Horde.update(iteration);
        Horde.draw();
        update_info();
        if (Horde.get_counter(HORDE_COUNTER_ON) + Horde.get_counter(HORDE_COUNTER_OFF) == 0) {
          score += Math.floor(level * efficiency / iteration);
          state = STATE_BETWEEN;
        }
        if ((Network.get_counter(NETWORK_COUNTER_BASE) + Network.get_counter(NETWORK_COUNTER_OFF)) <= 1) {
          state = STATE_END;
        }
        break;
      case STATE_END:
        UI.set_cursor(null);
        UI.clear();
        Network.toasters();
        Network.draw();
        // UI.refresh(); // Don't refresh, the main loop will do it
        UI.popup_dialog(DIALOG_ENDGAME);
        // Moved as callbacks to the modal dialogs because they are asynchronous
        /*
         if (Scorelist.ishighscore(score)) {
         UI.popup_dialog(DIALOG_ENTERNAME);
         Scorelist.update();
         }
         UI.popup_dialog(DIALOG_HIGHSCORE);
         Game.draw_logo();
         */
        UI.kill_timer();
        UI.set_pausebutton(false);
        state = STATE_WAITING;
        break;
      case STATE_BETWEEN:
        UI.set_cursor(defaultcursor);
        var str = "After Level {0}:\nScore: {1}".format(level, score);
        UI.update_dialog(DIALOG_SCORE, str);
        UI.popup_dialog(DIALOG_SCORE);
        // Moved as callbacks to the modal dialogs because they are asynchronous
        /*
         state = STATE_PLAYING;
         setup_level(++level);
         */
        break;
    }
    // UI.refresh(); // Don't refresh, the main loop will do it
    iteration++;
  };

  Game.score = function () {
    return score;
  };

  Game.level = function () {
    return level;
  };

  Game.screensize = function () {
    return screensize;
  };

  Game.scale = function (dimensions) {
    var scale = screensize / SCREENSIZE;
    var d = 1;
    for (; dimensions > 0; dimensions--) {
      d *= scale;
    }
    return d;
  };

  /*
   * Added because of asynchronous JS
   */

  Game.cb_after_newgame = function() {
    Game.start(1);
  };

  Game.cb_after_quitgame = function(){
    Game.quit();
  };

  Game.cb_after_warplevel = function (level) {
    level = parseInt(level);
    if (level) {
      Game.warp_to_level(parseInt(level));
    }
  };

  Game.cb_after_score = function () {
    state = STATE_PLAYING;
    setup_level(++level);
  };

  Game.cb_after_end = function () {
    if (Scorelist.ishighscore(score)) {
      UI.popup_dialog(DIALOG_ENTERNAME);
    }
    else {
      UI.popup_dialog(DIALOG_HIGHSCORE);
    }
  };

  Game.cb_after_entername = function (name) {
    Game.add_high_score(name);
    Scorelist.update();
    UI.popup_dialog(DIALOG_HIGHSCORE);
  };

  Game.cb_after_highscores = function () {
    if (state == STATE_WAITING) {
      Game.draw_logo();
    }
  };

  Game.main = function () {
    // srand(time(null))  // No seed initialization
    UI.initialize();
    UI.make_main_window(screensize);
    logo = UI.load_picture("logo");

    Scorelist.read();
    Scorelist.update();

    defaultcursor = UI.load_cursor("hand_up");
    downcursor = UI.load_cursor("hand_down");
    UI.set_cursor(null);

    Bill.load_pix();
    OS.load_pix();
    Computer.load_pix();
    Bucket.load_pix();
    Spark.load_pix();
    Sound.load();

    UI.set_pausebutton(false);

    // Wait for assets to load and Allegro to initialize
    ready(function () {
      Game.draw_logo();
      UI.refresh();
      // Handle the UI main loop here, we need the game state
      loop(function () {
        switch (state) {
          case STATE_WAITING:
            // Title screen
            break;
          default:
            if (mouse_released) {  // Inverted!
              Game.button_press(mouse_x, mouse_y);
            }
            else if (mouse_pressed) {  // Inverted!
              Game.button_release(mouse_x, mouse_y);
            }
            UI.refresh();
        }
      }, BPS_TO_TIMER(60));  // This is the frequency of the screen refresh, not the game rhythm
    });
  };

})(this);


// Global main function for Allegro
function main() {
  Game.main();

  return 0;
}

END_OF_MAIN();
