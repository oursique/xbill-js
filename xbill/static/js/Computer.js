var COMPUTER_TOASTER = 0;

(function (exports) {

  /* offset of screen from 0,0 */
  var OS_OFFSET = 4;

  var MIN_PC = 6;
  /* type >= MIN_PC means the computer is a PC */

  var cpuname = ["toaster", "maccpu", "nextcpu", "sgicpu", "suncpu", "palmcpu", "os2cpu", "bsdcpu"];

  var pictures = new Array(cpuname.length);  // array of cpu pictures

  function get_border(size) {
    return Math.floor(size / 10);  // at least this far from a side
  }

  var Computer = function () {
    /* CPU type */
    this.type = 0;
    /* current OS */
    this.os = 0;
    /* location */
    this.x = 0;
    this.y = 0;
    /* is the computer being used? */
    this.busy = 0;
    this.stray = null;
  };
  exports.Computer = Computer;

  Computer.setup = function (index) {
    var counter = 0, flag = 0;
    var x = 0, y = 0;
    var screensize = Game.screensize();
    var border = get_border(screensize);
    var width = Computer.width();
    var height = Computer.height();

    var computer = new Computer();

    do {
      if (++counter > 4000) {
        log("too many computers: " + counter);
        throw counter;
      }
      x = RAND(border, screensize - border - width);
      y = RAND(border, screensize - border - height);
      flag = true;
      /* check for conflicting computer placement */
      for (var j = 0; j < index && flag; j++) {
        var c = Network.get_computer(j);
        var twidth = width - BILL_OFFSET_X + Bill.width();
        if (UI.intersect(x, y, twidth, height, c.x, c.y, twidth, height)) {
          flag = false;
        }
      }
    } while (!flag);

    computer.x = x;
    computer.y = y;
    computer.type = RAND(1, cpuname.length - 1);
    computer.os = computer.determineOS();
    computer.busy = false;
    computer.stray = null;

    return computer;
  };

  Computer.prototype.determineOS = function () {
    if (this.type < MIN_PC) {
      return this.type;
    }
    else {
      return OS.randpc();
    }
  };

  Computer.prototype.on = function (locx, locy) {
    return (abs(locx - this.x) < Computer.width() && abs(locy - this.y) < Computer.height());
  };

  Computer.prototype.compatible = function (system) {
    return (this.type == system || (this.type >= MIN_PC && OS.ispc(system)));
  };

  Computer.prototype.draw = function () {
    UI.draw(pictures[this.type], this.x, this.y);
    if (this.os != OS_OFF) {
      OS.draw(this.os, this.x + OS_OFFSET, this.y + OS_OFFSET);
    }
  };

  Computer.load_pix = function () {
    for (var i = 0; i < cpuname.length; i++) {
      pictures[i] = UI.load_picture(cpuname[i]);
    }
  };

  Computer.width = function () {
    return UI.picture_width(pictures[0]);
  };

  Computer.height = function () {
    return UI.picture_height(pictures[0]);
  }
})(this);
