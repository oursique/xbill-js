/* Counters */
var HORDE_COUNTER_OFF = 0;
var HORDE_COUNTER_ON = 1;
var HORDE_COUNTER_MAX = 1;

(function (exports) {
  var alive = [], strays = [];
  var counters = new Array(HORDE_COUNTER_MAX + 1);

  var MAX_BILLS = 100;
  /* max Bills per level */

  function on(lev) {
    var perlevel = Math.floor((8 + 3 * lev) * Game.scale(2));
    return Math.min(perlevel, MAX_BILLS);
  }

  function max_at_once(lev) {
    return Math.min(Math.floor(2 + lev / 4), 12);
  }

  function between(lev) {
    return Math.max(Math.floor(14 - lev / 3), 10);
  }

  /*  Launches Bills whenever called  */
  function launch(max) {
    var bill;
    var off_screen = counters[HORDE_COUNTER_OFF];

    if (max == 0 || off_screen == 0) {
      return;
    }
    var n = RAND(1, Math.min(max, off_screen));
    for (; n > 0; n--) {
      bill = Bill.enter();
      alive.unshift(bill);
    }
    Sound.play(SOUND_BILL_ENTER);
  }

  var Horde = {};
  exports.Horde = Horde;

  Horde.setup = function () {
    alive = [];
    strays = [];
    counters[HORDE_COUNTER_OFF] = on(Game.level());
    counters[HORDE_COUNTER_ON] = 0;
  };

  Horde.update = function (iteration) {
    var level = Game.level();
    if (iteration % between(level) == 0) {
      launch(max_at_once(level));
    }
    for (var i = 0; i < alive.length; i++) {
      alive[i].update();
    }
  };

  Horde.draw = function () {
    var i;

    for (i = 0; i < strays.length; i++) {
      strays[i].draw();
    }
    for (i = 0; i < alive.length; i++) {
      alive[i].draw();
    }
  };

  Horde.move_bill = function (bill) {
    alive.splice(alive.indexOf(bill), 1);
    strays.unshift(bill);
  };

  Horde.remove_bill = function (bill) {
    if (bill.state == BILL_STATE_STRAY) {
      strays.splice(strays.indexOf(bill), 1);
    }
    else {
      alive.splice(alive.indexOf(bill), 1);
    }
    Network.clear_stray(bill);
  };

  Horde.add_bill = function (bill) {
    if (bill.state == BILL_STATE_STRAY) {
      strays.unshift(bill);
    }
    else {
      alive.unshift(bill);
    }
  };

  Horde.clicked_stray = function (x, y) {
    var bill;

    for (var i = 0; i < strays.length; i++) {
      bill = strays[i];
      if (!bill.clickedstray(x, y)) {
        continue;
      }
      strays.splice(strays.indexOf(bill), 1);
      return bill;
    }
    return null;
  };

  Horde.process_click = function (x, y) {
    var bill;
    var counter = 0;

    for (var i = 0; i < alive.length; i++) {
      bill = alive[i];
      if (bill.state == BILL_STATE_DYING || !bill.clicked(x, y)) {
        continue;
      }
      if (bill.state == BILL_STATE_AT) {
        var comp = Network.get_computer(bill.target_c);
        comp.busy = false;
        comp.stray = bill;
      }
      bill.set_dying();
      Sound.play(SOUND_BILL_KILL);
      counter++;
    }
    return counter;
  };

  Horde.inc_counter = function (counter, val) {
    counters[counter] += val;
  };

  Horde.get_counter = function (counter) {
    return counters[counter];
  };
})(this);
