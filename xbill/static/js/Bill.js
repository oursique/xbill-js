/* Bill's states */
var BILL_STATE_IN = 1;
var BILL_STATE_AT = 2;
var BILL_STATE_OUT = 3;
var BILL_STATE_DYING = 4;
var BILL_STATE_STRAY = 5;

/* Offsets from upper right of computer */
var BILL_OFFSET_X = 20;
var BILL_OFFSET_Y = 3;

(function (exports) {
  /* speed at which OS drops */
  var GRAVITY = 3;

  /* speed of moving Bill */
  var SLOW = 0;
  var FAST = 1;

  var WCELS = 4;  // # of bill walking animation frames
  var DCELS = 5;  // # of bill dying animation frames
  var ACELS = 13;  // # of bill switching OS frames

  var lcels = new Array(WCELS), rcels = new Array(WCELS), acels = new Array(ACELS), dcels = new Array(DCELS);

  function get_border() {
    var x = 0, y = 0;
    var i = RAND(0, 3);
    var screensize = Game.screensize();
    var width = Bill.width();
    var height = Bill.height();

    if (i % 2 == 0) {
      x = RAND(0, screensize - width);
    }
    else {
      y = RAND(0, screensize - height);
    }

    switch (i) {
      case 0:
        y = -height - 16;
        break;
      case 1:
        x = screensize + 1;
        break;
      case 2:
        y = screensize + 1;
        break;
      case 3:
        x = -width - 2;
        break;
    }

    return {x: x, y: y};
  }

  var Bill = function () {
    /* what is it doing? */
    this.state = 0;
    /* index of animation frame */
    this.index = 0;
    /* array of animation frames */
    this.cels = undefined;
    /* location */
    this.x = 0;
    this.y = 0;
    /* target x position */
    this.target_x = 0;
    /* target y position */
    this.target_y = 0;
    /* target computer */
    this.target_c = 0;
    /* which OS carried */
    this.cargo = 0;
    /* accounts for width differences */
    this.x_offset = 0;
    /* 'bounce' factor for OS carried */
    this.y_offset = 0;
    /* used for drawing extra OS during switch */
    this.sx = 0;
    this.sy = 0;
  };
  exports.Bill = Bill;

  /* Adds a bill to the in state */
  Bill.enter = function () {
    var bill = new Bill();

    bill.state = BILL_STATE_IN;
    var border = get_border();
    bill.x = border.x;
    bill.y = border.y;
    bill.index = 0;
    bill.cels = lcels;
    bill.cargo = OS_WINGDOWS;
    bill.x_offset = -2;
    bill.y_offset = -15;
    bill.target_c = RAND(0, Network.num_computers() - 1);
    var computer = Network.get_computer(bill.target_c);
    bill.target_x = computer.x + Computer.width() - BILL_OFFSET_X;
    bill.target_y = computer.y + BILL_OFFSET_Y;
    Horde.inc_counter(HORDE_COUNTER_ON, 1);
    Horde.inc_counter(HORDE_COUNTER_OFF, -1);
    return bill;
  };

  function step_size(level) {
    return Math.min(11 + level, 15);
  }

  /*  Moves bill toward his target - returns whether or not he moved */
  Bill.prototype.move = function (mode) {
    var xdist = this.target_x - this.x;
    var ydist = this.target_y - this.y;
    var step = step_size(Game.level());
    var dx = 0, dy = 0;
    var signx = xdist >= 0 ? 1 : -1;
    var signy = ydist >= 0 ? 1 : -1;
    xdist = abs(xdist);
    ydist = abs(ydist);
    if (!xdist && !ydist) {
      return 0;
    }
    else if (xdist < step && ydist < step) {
      this.x = this.target_x;
      this.y = this.target_y;
    }
    else {
      dx = Math.floor(xdist * step * signx) / (xdist + ydist);
      dy = Math.floor(ydist * step * signy) / (xdist + ydist);
      if (mode == FAST) {
        dx *= 1.25;
        dy *= 1.25;
      }
      this.x += dx;
      this.y += dy;
      if (dx < 0) {
        this.cels = lcels;
      }
      else if (dx > 0) {
        this.cels = rcels;
      }
    }
    return 1;
  };

  Bill.prototype.draw_std = function () {
    if (this.cargo >= 0) {
      OS.draw(this.cargo, this.x + this.x_offset, this.y + this.y_offset);
    }
    UI.draw(this.cels[this.index], this.x, this.y);
  };

  Bill.prototype.draw_at = function () {
    var computer = Network.get_computer(this.target_c);
    if (this.index > 6 && this.index < 12) {
      OS.draw(0, this.x + this.sx, this.y + this.sy);
    }
    if (this.cargo >= 0) {
      OS.draw(this.cargo, this.x + this.x_offset, this.y + this.y_offset);
    }
    UI.draw(this.cels[this.index], computer.x, computer.y);
  };

  Bill.prototype.draw_stray = function () {
    OS.draw(this.cargo, this.x, this.y);
  };

  Bill.prototype.draw = function () {
    switch (this.state) {
      case BILL_STATE_IN:
      case BILL_STATE_OUT:
      case BILL_STATE_DYING:
        this.draw_std();
        break;
      case BILL_STATE_AT:
        this.draw_at();
        break;
      case BILL_STATE_STRAY:
        this.draw_stray();
        break;
      default:
        break;
    }
  };

  /*  Update Bill's position */
  Bill.prototype.update_in = function () {
    var moved = this.move(SLOW);
    var computer = Network.get_computer(this.target_c);
    if (!moved && computer.os != OS_WINGDOWS && !computer.busy) {
      computer.busy = true;
      this.cels = acels;
      this.index = 0;
      this.state = BILL_STATE_AT;
      return;
    }
    else if (!moved) {
      var i;
      do {
        i = RAND(0, Network.num_computers() - 1);
      } while (i == this.target_c);
      computer = Network.get_computer(i);
      this.target_c = i;
      this.target_x = computer.x + Computer.width() - BILL_OFFSET_X;
      this.target_y = computer.y + BILL_OFFSET_Y;
    }
    this.index++;
    this.index %= WCELS;
    this.y_offset += (8 * (this.index % 2) - 4);
  };

  /*  Update Bill standing at a computer */
  Bill.prototype.update_at = function () {
    var computer = Network.get_computer(this.target_c);
    if (this.index == 0 && computer.os == OS_OFF) {
      this.index = 6;
      if (this.stray == null) {
        this.cargo = -1;
      }
      else {
        this.cargo = computer.stray.cargo;
        Horde.remove_bill(computer.stray);
        computer.stray = null;
      }
    } else {
      this.index++;
    }
    if (this.index == 13) {
      this.y_offset = -15;
      this.x_offset = -2;
      var border = get_border();
      this.target_x = border.x;
      this.target_y = border.y;
      this.index = 0;
      this.cels = lcels;
      this.state = BILL_STATE_OUT;
      computer.busy = false;
      return;
    }
    this.y_offset = Bill.height() - OS.height();
    switch (this.index) {
      case 1:
      case 2:
        this.x -= 8;
        this.x_offset += 8;
        break;
      case 3:
        this.x -= 10;
        this.x_offset += 10;
        break;
      case 4:
        this.x += 3;
        this.x_offset -= 3;
        break;
      case 5:
        this.x += 2;
        this.x_offset -= 2;
        break;
      case 6:
        if (computer.os != OS_OFF) {
          Network.inc_counter(NETWORK_COUNTER_BASE, -1);
          Network.inc_counter(NETWORK_COUNTER_OFF, 1);
          this.cargo = computer.os;
        }
        else {
          this.x -= 21;
          this.x_offset += 21;
        }
        computer.os = OS_OFF;
        Sound.play(SOUND_OS_REMOVE);
        this.y_offset = -15;
        this.x += 20;
        this.x_offset -= 20;
        break;
      case 7:
        this.sy = this.y_offset;
        this.sx = -2;
        break;
      case 8:
        this.sy = -15;
        this.sx = -2;
        break;
      case 9:
        this.sy = -7;
        this.sx = -7;
        this.x -= 8;
        this.x_offset += 8;
        break;
      case 10:
        this.sy = 0;
        this.sx = -7;
        this.x -= 15;
        this.x_offset += 15;
        break;
      case 11:
        this.sy = 0;
        this.sx = -7;
        computer.os = OS_WINGDOWS;
        Sound.play(SOUND_WINGDOWS_INSTALL);
        Network.inc_counter(NETWORK_COUNTER_OFF, -1);
        Network.inc_counter(NETWORK_COUNTER_WIN, 1);
        break;
      case 12:
        this.x += 11;
        this.x_offset -= 11;
    }
  };

  /* Updates Bill fleeing with his ill gotten gain */
  Bill.prototype.update_out = function () {
    var screensize = Game.screensize();
    if (UI.intersect(this.x, this.y, Bill.width(), Bill.height(), 0, 0, screensize, screensize)) {
      this.move(FAST);
      this.index++;
      this.index %= WCELS;
      this.y_offset += (8 * (this.index % 2) - 4);
    }
    else {
      Horde.remove_bill(this);
      Horde.inc_counter(HORDE_COUNTER_ON, -1);
      Horde.inc_counter(HORDE_COUNTER_OFF, 1);
    }
  };


  /* Updates a Bill who is dying */
  Bill.prototype.update_dying = function () {
    if (this.index < DCELS - 1) {
      this.y_offset += (this.index * GRAVITY);
      this.index++;
    }
    else {
      this.y += this.y_offset;
      if (this.cargo < 0 || this.cargo == OS_WINGDOWS) {
        Horde.remove_bill(this);
      }
      else {
        Horde.move_bill(this);
        this.state = BILL_STATE_STRAY;
      }
      Horde.inc_counter(HORDE_COUNTER_ON, -1);
    }
  };

  Bill.prototype.update = function () {
    switch (this.state) {
      case BILL_STATE_IN:
        this.update_in();
        break;
      case BILL_STATE_AT:
        this.update_at();
        break;
      case BILL_STATE_OUT:
        this.update_out();
        break;
      case BILL_STATE_DYING:
        this.update_dying();
        break;
      default:
        break;
    }
  };

  Bill.prototype.set_dying = function () {
    this.index = 0;
    this.cels = dcels;
    this.x_offset = -2;
    this.y_offset = -15;
    this.state = BILL_STATE_DYING;
  };

  Bill.prototype.clicked = function (locx, locy) {
    return (locx > this.x && locx < this.x + Bill.width() && locy > this.y && locy < this.y + Bill.height());
  };

  Bill.prototype.clickedstray = function (locx, locy) {
    return (locx > this.x && locx < this.x + OS.width() && locy > this.y && locy < this.y + OS.height());
  };

  Bill.load_pix = function () {
    var i;

    for (i = 0; i < WCELS - 1; i++) {
      lcels[i] = UI.load_picture_indexed("billL", i, 1);
      rcels[i] = UI.load_picture_indexed("billR", i, 1);
    }
    lcels[WCELS - 1] = lcels[1];
    rcels[WCELS - 1] = rcels[1];

    for (i = 0; i < DCELS; i++) {
      dcels[i] = UI.load_picture_indexed("billD", i, 1);
    }

    for (i = 0; i < ACELS; i++) {
      acels[i] = UI.load_picture_indexed("billA", i, 1);
    }
  };

  Bill.width = function () {
    return UI.picture_width(dcels[0]);
  };

  Bill.height = function () {
    return UI.picture_height(dcels[0]);
  };
})(this);
