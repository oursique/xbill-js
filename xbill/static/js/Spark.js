var SPARK_SPEED = 4;

(function (exports) {
  var pictures = new Array(2);

  var Spark = {};
  exports.Spark = Spark;

  Spark.load_pix = function () {
    for (var i = 0; i < 2; i++) {
      pictures[i] = UI.load_picture_indexed("spark", i, 1);
    }
  };

  Spark.width = function () {
    return UI.picture_width(pictures[0]);
  };

  Spark.height = function () {
    return UI.picture_height(pictures[0]);
  };

  Spark.draw = function (x, y, index) {
    UI.draw(pictures[index], x, y);
  };
})(this);
