from django.core.management.base import NoArgsCommand

from xbill import models


class Command(NoArgsCommand):
    def delete_extra_scores(self, verbose):
        scores_saved = models.Score.objects.count()
        if verbose:
            self.stdout.write("Currently %s scores saved." % scores_saved)

        if scores_saved > 10:
            extra_scores_pks = models.Score.objects.order_by("-score")[10:].values_list(
                "pk", flat=True
            )
            extra_scores_num = len(extra_scores_pks)
            models.Score.objects.filter(pk__in=extra_scores_pks).delete()
            if verbose:
                self.stdout.write("Deleted %s extra score." % extra_scores_num)

        else:
            if verbose:
                self.stdout.write("No extra score to delete.")

    def handle_noargs(self, **options):
        verbose = options["verbosity"] > 0

        self.delete_extra_scores(verbose)

        # This is where we would reset scores to discourage cheats and offensive names
        # Say every monday at 00:00 UTC
        # self.reset_scores(verbose)
