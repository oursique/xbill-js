import json

from django.http.response import HttpResponse, HttpResponseBadRequest
from django.views.generic import FormView

from . import forms
from . import models


class HttpResponseCreated(HttpResponse):
    status_code = 201

    def __init__(self, *args, **kwargs):
        content = kwargs.pop("content", b"Created")
        super().__init__(content=content, *args, **kwargs)


class ScorefileView(FormView):
    form_class = forms.ScoreForm

    def form_valid(self, form):
        lowest_score = models.Score.objects.lowest_score()
        if form.cleaned_data["score"] > lowest_score:
            form.save()

        # Don't look if we have more than 10 scores, a single process will be responsible for cleaning them

        return HttpResponseCreated()

    def form_invalid(self, form):
        return HttpResponseBadRequest()

    def get(self, request, *args, **kwargs):
        high_scores = models.Score.objects.high_scores().values(
            "name", "level", "score"
        )
        # Directly usable as the new scores array
        dumped = json.dumps(list(high_scores))

        return HttpResponse(dumped, content_type="application/json")
