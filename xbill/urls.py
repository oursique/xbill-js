from django.conf.urls import url
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(
        r"^$",
        ensure_csrf_cookie(TemplateView.as_view(template_name="index.html")),
        name="index",
    ),
    url(r"^scorefile/$", views.ScorefileView.as_view(), name="scorefile"),
]
