PACKAGE=xbill
DJANGO_SETTINGS_MODULE="$(PACKAGE).settings"

all:

clean:
	find $(PACKAGE) -name __pycache__ -delete
	find $(PACKAGE) -type d -empty -delete
	grunt clean

prepare:
	grunt

collectstatic:
	DJANGO_SETTINGS_MODULE=$(DJANGO_SETTINGS_MODULE) django-admin collectstatic --noinput -v0

update:
	pip install -e .
	npm ci

deploy: update prepare collectstatic

.PHONY: clean prepare collectstatic update deploy
