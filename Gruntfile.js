/* global module */

module.exports = function(grunt) {
  'use strict';

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.initConfig({
    clean: {
      base: [
        'staticfiles/**',
      ]
    },
    copy: {
      jquery: {
        expand: true,
        cwd: 'node_modules/jquery/dist',
        src: [
          '*.min.js',
          '*.map'
        ],
        dest: 'staticfiles/js',
        nonull: true
      },
      bootstrap: {
        expand: true,
        cwd: 'node_modules/bootstrap/dist',
        src: [
            'css/*.min.css',
            'css/*.map',
            'js/*.min.js',
            'fonts/**'
        ],
        dest: 'staticfiles',
        nonull: true
      },
    },
  });

  grunt.registerTask('default', [
    'clean',
    'copy',
  ]);
};
